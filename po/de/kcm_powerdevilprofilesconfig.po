# SPDX-FileCopyrightText: 2010, 2023, 2024 Johannes Obermayr <johannesobermayr@gmx.de>
# Panagiotis Papadopoulos <pano_90@gmx.net>, 2010.
# Frederik Schwarzer <schwarzer@kde.org>, 2010, 2011.
# Burkhard Lück <lueck@hube-lueck.de>, 2020.
# SPDX-FileCopyrightText: 2024 Frank Steinmetzger <dev-kde@felsenfleischer.de>
msgid ""
msgstr ""
"Project-Id-Version: powerdevilprofilesconfig\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-18 00:41+0000\n"
"PO-Revision-Date: 2024-04-04 21:11+0200\n"
"Last-Translator: Frank Steinmetzger <dev-kde@felsenfleischer.de>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.02.1\n"

#: PowerKCM.cpp:455
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "Der Energieverwaltungsdienst scheint nicht zu laufen."

#: ui/DurationPromptDialog.qml:130
#, kde-format
msgctxt "The unit of the time input field"
msgid "millisecond"
msgid_plural "milliseconds"
msgstr[0] ""
msgstr[1] ""

#: ui/DurationPromptDialog.qml:132
#, kde-format
msgctxt "The unit of the time input field"
msgid "second"
msgid_plural "seconds"
msgstr[0] ""
msgstr[1] ""

#: ui/DurationPromptDialog.qml:134
#, kde-format
msgctxt "The unit of the time input field"
msgid "minute"
msgid_plural "minutes"
msgstr[0] ""
msgstr[1] ""

#: ui/DurationPromptDialog.qml:136
#, kde-format
msgctxt "The unit of the time input field"
msgid "hour"
msgid_plural "hours"
msgstr[0] ""
msgstr[1] ""

#: ui/DurationPromptDialog.qml:138
#, kde-format
msgctxt "The unit of the time input field"
msgid "day"
msgid_plural "days"
msgstr[0] ""
msgstr[1] ""

#: ui/DurationPromptDialog.qml:140
#, kde-format
msgctxt "The unit of the time input field"
msgid "week"
msgid_plural "weeks"
msgstr[0] ""
msgstr[1] ""

#: ui/DurationPromptDialog.qml:142
#, kde-format
msgctxt "The unit of the time input field"
msgid "month"
msgid_plural "months"
msgstr[0] ""
msgstr[1] ""

#: ui/DurationPromptDialog.qml:144
#, kde-format
msgctxt "The unit of the time input field"
msgid "year"
msgid_plural "years"
msgstr[0] ""
msgstr[1] ""

#: ui/DurationPromptDialog.qml:171
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "milliseconds"
msgstr ""

#: ui/DurationPromptDialog.qml:173
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "seconds"
msgstr ""

#: ui/DurationPromptDialog.qml:175
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "minutes"
msgstr ""

#: ui/DurationPromptDialog.qml:177
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "hours"
msgstr ""

#: ui/DurationPromptDialog.qml:179
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "days"
msgstr ""

#: ui/DurationPromptDialog.qml:181
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "weeks"
msgstr ""

#: ui/DurationPromptDialog.qml:183
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "months"
msgstr ""

#: ui/DurationPromptDialog.qml:185
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "years"
msgstr ""

#: ui/GlobalConfig.qml:16
#, kde-format
msgctxt "@title"
msgid "Advanced Power Settings"
msgstr "Erweiterte Energieverwaltung"

#: ui/GlobalConfig.qml:21
#, kde-format
msgctxt ""
"Percentage value example, used for formatting battery levels in the power "
"management settings"
msgid "10%"
msgstr "10 %"

#: ui/GlobalConfig.qml:57
#, kde-format
msgctxt "@title:group"
msgid "Battery Levels"
msgstr "Akku-Ladestände"

#: ui/GlobalConfig.qml:65
#, kde-format
msgctxt ""
"@label:spinbox Low battery level percentage for the main power supply battery"
msgid "&Low level:"
msgstr "Niedriger &Ladestand:"

#: ui/GlobalConfig.qml:73
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Low battery level"
msgstr "Niedriger Akku-Ladestand"

#: ui/GlobalConfig.qml:98
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The battery charge will be considered low when it drops to this level. "
"Settings for low battery will be used instead of regular battery settings."
msgstr ""
"Der Ladestand wird als niedrig betrachtet, sobald er auf dieses Niveau "
"fällt. Die Einstellungen für niedrigen Ladestand haben dann Vorrang vor den "
"normalen Akku-Einstellungen."

#: ui/GlobalConfig.qml:105
#, kde-format
msgctxt ""
"@label:spinbox Critical battery level percentage for the main power supply "
"battery"
msgid "Cr&itical level:"
msgstr "Kr&itischer Ladestand:"

#: ui/GlobalConfig.qml:113
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Critical battery level"
msgstr "Kritischer Akku-Ladestand"

#: ui/GlobalConfig.qml:138
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The battery charge will be considered critical when it drops to this level. "
"After a brief warning, the system will automatically suspend or shut down, "
"according to the configured critical battery level action."
msgstr ""
"Der Akku-Ladestand wird als kritisch betrachtet, sobald er auf dieses Niveau "
"fällt. Nach einer kurzen Warnung wird das System abhängig von der "
"eingestellten Aktion automatisch in den Ruhezustand versetzt oder "
"heruntergefahren."

#: ui/GlobalConfig.qml:147
#, kde-format
msgctxt "@info:status"
msgid ""
"The action you had previously configured for reaching the critical battery "
"level is now unsupported on your system. Please select a different one."
msgstr ""
"Die Aktion, die Sie zuvor für das Erreichen des kritischen Ladestandes "
"eingestellt haben, wird von Ihrem System nicht mehr unterstützt. Bitte "
"wählen Sie eine andere aus."

#: ui/GlobalConfig.qml:154
#, kde-format
msgctxt ""
"@label:combobox Power action such as sleep/hibernate that will be executed "
"when the critical battery level is reached"
msgid "A&t critical level:"
msgstr "Bei kri&tischem Ladestand:"

#: ui/GlobalConfig.qml:156
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action performed at critical battery level"
msgstr "Durchzuführende Aktion bei kritischem Ladestand"

#: ui/GlobalConfig.qml:181
#, kde-format
msgctxt "@label:spinbox Low battery level percentage for peripheral devices"
msgid "Low level for peripheral d&evices:"
msgstr "Niedriger Stand bei angeschlossenen &Geräten:"

#: ui/GlobalConfig.qml:189
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Low battery level for peripheral devices"
msgstr "Niedriger Batterie-Ladestand bei angeschlossenen &Geräten"

#: ui/GlobalConfig.qml:214
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The battery charge for peripheral devices will be considered low when it "
"reaches this level."
msgstr ""
"Der Batterie-Ladestand bei angeschlossenen Geräten wird als niedrig "
"betrachtet, sobald er dieses Niveau erreicht."

#: ui/GlobalConfig.qml:222
#, kde-format
msgctxt "@title:group"
msgid "Charge Limit"
msgstr "Ladebegrenzung"

#: ui/GlobalConfig.qml:233
#, kde-format
msgctxt "@label:checkbox"
msgid "&Battery protection:"
msgstr ""

#: ui/GlobalConfig.qml:234
#, kde-format
msgctxt "@text:checkbox"
msgid "Limit the maximum battery charge"
msgstr ""

#: ui/GlobalConfig.qml:242
#, kde-format
msgctxt ""
"@label:spinbox Battery will stop charging when this charge percentage is "
"reached"
msgid "&Stop charging at:"
msgstr "Laden b&eenden bei:"

#: ui/GlobalConfig.qml:268
#, kde-format
msgctxt ""
"@label:spinbox Battery will start charging again when this charge percentage "
"is reached, after having hit the stop-charging threshold earlier"
msgid "Start charging once &below:"
msgstr "Laden &beginnen unter:"

#: ui/GlobalConfig.qml:325
#, kde-format
msgctxt "@info:status"
msgid ""
"You might have to disconnect and re-connect the power source to start "
"charging the battery again."
msgstr ""
"Sie müssen möglicherweise die Stromquelle trennen und neu verbinden, um den "
"Ladevorgang neuzustarten."

#: ui/GlobalConfig.qml:336
#, kde-format
msgctxt "@info"
msgid ""
"Regularly charging the battery close to 100%, or fully discharging it, may "
"accelerate deterioration of battery health. By limiting the maximum battery "
"charge, you can help extend the battery lifespan."
msgstr ""
"Es kann die Alterung des Akkus beschleunigen, wenn er regelmäßig auf nahe "
"100 % geladen oder fast komplett entladen wird. Das Begrenzen der maximalen "
"Ladung kann helfen, die Lebensdauer des Akkus zu verlängern."

#: ui/GlobalConfig.qml:344
#, kde-format
msgctxt ""
"@title:group Miscellaneous power management settings that didn't fit "
"elsewhere"
msgid "Other Settings"
msgstr "Andere Einstellungen"

#: ui/GlobalConfig.qml:352
#, kde-format
msgctxt "@label:checkbox"
msgid "&Media playback:"
msgstr "&Medienwiedergabe:"

#: ui/GlobalConfig.qml:353
#, kde-format
msgctxt "@text:checkbox"
msgid "Pause media players when suspending"
msgstr "Medienspieler bei Standby/Ruhezustand anhalten"

#: ui/GlobalConfig.qml:366
#, kde-format
msgctxt "@label:button"
msgid "Related pages:"
msgstr "Verwandte Einstellungen:"

#: ui/GlobalConfig.qml:381
#, kde-format
msgctxt "@text:button Name of KCM, plus Power Management notification category"
msgid "Notifications: Power Management"
msgstr "Benachrichtigungen: Energieverwaltung"

#: ui/GlobalConfig.qml:383
#, kde-format
msgid "Open \"Notifications\" settings page, \"Power Management\" section"
msgstr "Öffnen der Seite „Benachrichtigungen“, Abschnitt „Energieverwaltung“"

#: ui/GlobalConfig.qml:390
#, kde-format
msgctxt "@text:button Name of KCM"
msgid "Desktop Session"
msgstr "Arbeitsflächen-Sitzung"

#: ui/GlobalConfig.qml:391
#, kde-format
msgid "Open \"Desktop Session\" settings page"
msgstr "Öffnen der Seite „Arbeitsflächen-Sitzung“"

#: ui/GlobalConfig.qml:398
#, kde-format
msgctxt "@text:button Name of KCM"
msgid "Activities"
msgstr "Aktivitäten"

#: ui/GlobalConfig.qml:399
#, kde-format
msgid "Open \"Activities\" settings page"
msgstr "Öffnen der Seite „Aktivitäten“"

#: ui/main.qml:19
#, kde-format
msgctxt "@action:button"
msgid "Advanced Power &Settings…"
msgstr "Erweiterte Energieein&stellungen …"

#: ui/main.qml:43
#, kde-format
msgctxt "@text:placeholdermessage"
msgid "Power Management settings could not be loaded"
msgstr "Energieverwaltungseinstellungen können nicht geladen werden"

#: ui/main.qml:65
#, kde-format
msgid "On AC Power"
msgstr "Am Netzkabel"

#: ui/main.qml:66
#, kde-format
msgid "On Battery"
msgstr "Im Akkubetrieb"

#: ui/main.qml:67
#, kde-format
msgid "On Low Battery"
msgstr "Im Akkubetrieb bei niedrigem Ladestand"

#: ui/ProfileConfig.qml:24
#, kde-format
msgctxt ""
"Percentage value example, used for formatting brightness levels in the power "
"management settings"
msgid "10%"
msgstr "10 %"

#: ui/ProfileConfig.qml:33
#, kde-format
msgctxt "@title:group"
msgid "Suspend Session"
msgstr "Sitzung in den Standby-Modus versetzen"

#: ui/ProfileConfig.qml:48
#, kde-format
msgctxt "@info:status"
msgid ""
"The action you had previously configured for after a period of inactivity is "
"now unsupported on your system. Please select a different one."
msgstr ""
"Die Aktion, die Sie zuvor für eine Weile Inaktivität eingestellt haben, wird "
"von Ihrem System nicht mehr unterstützt. Bitte wählen Sie eine andere aus."

#: ui/ProfileConfig.qml:55
#, kde-format
msgctxt ""
"@label:combobox Suspend action such as sleep/hibernate to perform when the "
"system is idle"
msgid "A&fter a period of inactivity:"
msgstr "Nach einer Weile &Inaktivität:"

#: ui/ProfileConfig.qml:63
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action to perform when the system is idle"
msgstr "Aktion, die ausgeführt werden soll, wenn das System nichts tut"

#: ui/ProfileConfig.qml:95
#, fuzzy, kde-format
#| msgid "after %1 sec"
#| msgid_plural "after %1 sec"
msgctxt "@option:combobox"
msgid "after %1 second"
msgid_plural "after %1 seconds"
msgstr[0] "nach %1 Sek."
msgstr[1] "nach %1 Sek."

#: ui/ProfileConfig.qml:97
#, fuzzy, kde-format
#| msgid "after %1 min"
#| msgid_plural "after %1 min"
msgctxt "@option:combobox"
msgid "after %1 minute"
msgid_plural "after %1 minutes"
msgstr[0] "nach %1 Min."
msgstr[1] "nach %1 Min."

#: ui/ProfileConfig.qml:110 ui/ProfileConfig.qml:383 ui/ProfileConfig.qml:526
#: ui/ProfileConfig.qml:862
#, kde-format
msgctxt ""
"@option:combobox Choose a custom value outside the list of preset values"
msgid "Custom…"
msgstr ""

#: ui/ProfileConfig.qml:148
#, kde-format
msgctxt "@info:status"
msgid ""
"The action you had previously configured for when the power button is "
"pressed is now unsupported on your system. Please select a different one."
msgstr ""
"Die Aktion, die Sie zuvor für das Drücken das Einschaltknopfes eingestellt "
"haben, wird von Ihrem System nicht mehr unterstützt. Bitte wählen Sie eine "
"andere aus."

#: ui/ProfileConfig.qml:155
#, kde-format
msgctxt ""
"@label:combobox Suspend action such as sleep/hibernate to perform when the "
"power button is pressed"
msgid "When &power button pressed:"
msgstr "Wenn der &Einschaltknopf gedrückt wird:"

#: ui/ProfileConfig.qml:157
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action to perform when the power button is pressed"
msgstr ""
"Aktion, die ausgeführt werden soll, wenn der Einschaltknopf gedrückt wird"

#: ui/ProfileConfig.qml:187
#, kde-format
msgctxt "@info:status"
msgid ""
"The action you had previously configured for when the lid is closed is now "
"unsupported on your system. Please select a different one."
msgstr ""
"Die Aktion, die Sie zuvor für das Schließen des Deckels eingestellt haben, "
"wird von Ihrem System nicht mehr unterstützt. Bitte wählen Sie eine andere "
"aus."

#: ui/ProfileConfig.qml:194
#, kde-format
msgctxt ""
"@label:combobox Suspend action such as sleep/hibernate to perform when the "
"power button is pressed"
msgid "When laptop &lid closed:"
msgstr "Wenn der Laptop&deckel geschlossen wird:"

#: ui/ProfileConfig.qml:196
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action to perform when the laptop lid is closed"
msgstr ""
"Aktion, die ausgeführt werden soll, wenn der Laptopdeckel geschlossen wird"

#: ui/ProfileConfig.qml:225
#, kde-format
msgctxt ""
"@text:checkbox Trigger laptop lid action even when an external monitor is "
"connected"
msgid "Even when an external monitor is connected"
msgstr "Selbst wenn ein externer Monitor angeschlossen ist"

#: ui/ProfileConfig.qml:227
#, kde-format
msgid "Perform laptop lid action even when an external monitor is connected"
msgstr ""
"Die Aktion beim Schließen des Laptopdeckels auch dann ausführen, wenn ein "
"externer Monitor angeschlossen ist"

#: ui/ProfileConfig.qml:245
#, kde-format
msgctxt "@info:status"
msgid ""
"The sleep mode you had previously configured is now unsupported on your "
"system. Please select a different one."
msgstr ""
"Der Ruhezustandsmodus, den Sie zuvor ausgewählt haben, wird von Ihrem System "
"nicht mehr unterstützt. Bitten wählen Sie einen anderen aus."

#: ui/ProfileConfig.qml:252
#, kde-format
msgctxt ""
"@label:combobox Sleep mode selection - suspend to memory, disk or both"
msgid "When sleeping, enter:"
msgstr "Energiesparmodus:"

#: ui/ProfileConfig.qml:254
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "When sleeping, enter this power-save mode"
msgstr "Diesen Energiesparmodus verwenden"

#: ui/ProfileConfig.qml:303
#, kde-format
msgctxt "@title:group"
msgid "Display and Brightness"
msgstr "Anzeige und Helligkeit"

#: ui/ProfileConfig.qml:313
#, kde-format
msgctxt "@label:slider Brightness level"
msgid "Change scr&een brightness:"
msgstr "Bildschirm&helligkeit ändern:"

#: ui/ProfileConfig.qml:359
#, fuzzy, kde-format
#| msgctxt "@label:spinbox Dim screen after X minutes"
#| msgid "Di&m automatically:"
msgctxt "@label:combobox Dim screen after X minutes"
msgid "Di&m automatically:"
msgstr "Automatisch abdun&keln:"

#: ui/ProfileConfig.qml:362
#, fuzzy, kde-format
#| msgctxt "@label:spinbox Dim screen after X minutes"
#| msgid "Di&m automatically:"
msgctxt "@label:spinbox Dim screen after X minutes"
msgid "Di&m automatically after:"
msgstr "Automatisch abdun&keln:"

#: ui/ProfileConfig.qml:367
#, kde-format
msgctxt "@option:combobox"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] ""
msgstr[1] ""

#: ui/ProfileConfig.qml:369
#, kde-format
msgctxt "@option:combobox"
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] ""
msgstr[1] ""

#: ui/ProfileConfig.qml:376
#, kde-format
msgctxt "@option:combobox Dim screen automatically"
msgid "Never"
msgstr ""

#: ui/ProfileConfig.qml:428
#, kde-format
msgctxt "@info:status"
msgid ""
"The screen will not be dimmed because it is configured to turn off sooner."
msgstr ""

#: ui/ProfileConfig.qml:433
#, fuzzy, kde-format
#| msgctxt "@label:spinbox After X minutes"
#| msgid "&Turn off screen:"
msgctxt "@label:combobox After X minutes"
msgid "&Turn off screen:"
msgstr "Bildschirm &ausschalten:"

#: ui/ProfileConfig.qml:446
#, fuzzy, kde-format
#| msgctxt "@label:spinbox After X minutes"
#| msgid "&Turn off screen:"
msgctxt "@label:spinbox After X minutes"
msgid "Turn off screen after:"
msgstr "Bildschirm &ausschalten:"

#: ui/ProfileConfig.qml:450
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] ""
msgstr[1] ""

#: ui/ProfileConfig.qml:452
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] ""
msgstr[1] ""

#: ui/ProfileConfig.qml:459
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "Never"
msgstr ""

#: ui/ProfileConfig.qml:466
#, kde-format
msgctxt ""
"@option:combobox Choose a custom value outside the list of preset values "
"(caution: watch for string length)"
msgid "Custom…"
msgstr ""

#: ui/ProfileConfig.qml:506
#, fuzzy, kde-format
#| msgctxt "@label:spinbox After X seconds"
#| msgid "When loc&ked, turn off screen:"
msgctxt "@label:spinbox After X minutes"
msgid "When locked, turn off screen after:"
msgstr "Wenn gesperrt, Bildschirm ausschalten:"

#: ui/ProfileConfig.qml:510
#, fuzzy, kde-format
#| msgctxt "@label:spinbox After X seconds"
#| msgid "When loc&ked, turn off screen:"
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "When locked: %1 second"
msgid_plural "When locked: %1 seconds"
msgstr[0] "Wenn gesperrt, Bildschirm ausschalten:"
msgstr[1] "Wenn gesperrt, Bildschirm ausschalten:"

#: ui/ProfileConfig.qml:512
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "When locked: %1 minute"
msgid_plural "When locked: %1 minutes"
msgstr[0] ""
msgstr[1] ""

#: ui/ProfileConfig.qml:519
#, fuzzy, kde-format
#| msgctxt "@label:spinbox After X seconds"
#| msgid "When loc&ked, turn off screen:"
msgctxt ""
"@option:combobox Turn off screen after X minutes regardless of lock screen "
"(caution: watch for string length)"
msgid "When locked and unlocked"
msgstr "Wenn gesperrt, Bildschirm ausschalten:"

#: ui/ProfileConfig.qml:521
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "When locked: Immediately"
msgstr ""

#: ui/ProfileConfig.qml:560
#, kde-format
msgctxt "@label:slider Brightness level"
msgid "Change key&board brightness:"
msgstr "Tastat&urhelligkeit ändern:"

#: ui/ProfileConfig.qml:607
#, kde-format
msgctxt "@title:group"
msgid "Other Settings"
msgstr "Andere Einstellungen"

#: ui/ProfileConfig.qml:615
#, kde-format
msgctxt ""
"@label:combobox Power Save, Balanced or Performance profile - same options "
"as in the Battery applet"
msgid "Switch to po&wer profile:"
msgstr "Energiep&rofil wechseln zu:"

#: ui/ProfileConfig.qml:619
#, kde-format
msgctxt ""
"@accessible:name:combobox Power Save, Balanced or Performance profile - same "
"options as in the Battery applet"
msgid "Switch to power profile"
msgstr "Das Energieprofil wechseln"

#: ui/ProfileConfig.qml:646
#, kde-format
msgctxt "@label:button"
msgid "Run custom script:"
msgstr "Eigenes Skript ausführen:"

#: ui/ProfileConfig.qml:656
#, kde-format
msgctxt ""
"@text:button Determine what will trigger a script command to run in this "
"power state"
msgid "Choose run conditions…"
msgstr "Ausführungsbedingungen auswählen …"

#: ui/ProfileConfig.qml:658
#, kde-format
msgctxt "@accessible:name:button"
msgid "Choose run conditions for script command"
msgstr "Die Bedingungen für die Ausführung des Skripts auswählen"

#: ui/ProfileConfig.qml:677
#, fuzzy, kde-format
#| msgctxt "@text:action:menu Script command to execute"
#| msgid "When e&ntering this power state"
msgctxt ""
"@text:action:menu Script command to execute for power state (On AC Power, On "
"Battery, ...)"
msgid "When entering \"%1\" state"
msgstr "Beim &Wechseln in diesen Energiezustand"

#: ui/ProfileConfig.qml:695
#, fuzzy, kde-format
#| msgctxt "@text:action:menu Script command to execute"
#| msgid "When e&xiting this power state"
msgctxt ""
"@text:action:menu Script command to execute for power state (On AC Power, On "
"Battery, ...)"
msgid "When exiting \"%1\" state"
msgstr "Beim We&chseln aus diesem Energiezustand"

#: ui/ProfileConfig.qml:713
#, fuzzy, kde-format
#| msgctxt "@text:action:menu Script command to execute"
#| msgid "After a period of inacti&vity"
msgctxt "@text:action:menu Script command to execute"
msgid "After a period of inactivity"
msgstr "Nach einer Weile &Inaktivität"

#: ui/ProfileConfig.qml:736
#, fuzzy, kde-format
#| msgctxt "@label:textfield Script command to execute"
#| msgid "When e&ntering this power state:"
msgctxt ""
"@label:textfield Script command to execute for power state (On AC Power, On "
"Battery, ...)"
msgid "When e&ntering \"%1\" state:"
msgstr "Beim &Betreten dieses Energiezustands:"

#: ui/ProfileConfig.qml:740
#, fuzzy, kde-format
#| msgctxt "@label:textfield"
#| msgid "Script command when entering this power state"
msgctxt "@label:textfield for power state (On AC Power, On Battery, ...)"
msgid "Script command when entering \"%1\" state"
msgstr "Skriptbefehl beim Betreten dieses Energiezustands"

#: ui/ProfileConfig.qml:771
#, fuzzy, kde-format
#| msgctxt "@label:textfield Script command to execute"
#| msgid "When e&xiting this power state:"
msgctxt ""
"@label:textfield Script command to execute for power state (On AC Power, On "
"Battery, ...)"
msgid "When e&xiting \"%1\" state:"
msgstr "Beim &Verlassen dieses Energiezustands:"

#: ui/ProfileConfig.qml:775
#, fuzzy, kde-format
#| msgctxt "@label:textfield"
#| msgid "Script command when exiting this power state"
msgctxt "@label:textfield for power state (On AC Power, On Battery, ...)"
msgid "Script command when exiting \"%1\" state"
msgstr "Skriptbefehl beim Betreten dieses Energiezustands"

#: ui/ProfileConfig.qml:806
#, kde-format
msgctxt "@label:textfield Script command to execute"
msgid "After a period of inacti&vity:"
msgstr "Nach einer Weile &Inaktivität:"

#: ui/ProfileConfig.qml:808
#, kde-format
msgctxt "@@accessible:name:textfield"
msgid "Script command after a period of inactivity"
msgstr "Skriptbefehl beim Betreten dieses Energiezustands"

#: ui/ProfileConfig.qml:837
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Period of inactivity until the script command executes"
msgstr "Die Dauer der Inaktivität bevor das Skript ausgeführt wird"

#: ui/ProfileConfig.qml:842
#, kde-format
msgctxt "@label:spinbox After X minutes"
msgid "Execute script after:"
msgstr ""

#: ui/ProfileConfig.qml:847
#, fuzzy, kde-format
#| msgid "after %1 sec"
#| msgid_plural "after %1 sec"
msgctxt "@option:combobox"
msgid "after %1 seconds"
msgid_plural "after %1 seconds"
msgstr[0] "nach %1 Sek."
msgstr[1] "nach %1 Sek."

#: ui/ProfileConfig.qml:849
#, fuzzy, kde-format
#| msgid "after %1 min"
#| msgid_plural "after %1 min"
msgctxt "@option:combobox"
msgid "after %1 minutes"
msgid_plural "after %1 minutes"
msgstr[0] "nach %1 Min."
msgstr[1] "nach %1 Min."

#: ui/RunScriptEdit.qml:33
#, kde-format
msgid "Enter command or select file…"
msgstr "Geben Sie einen Befehl ein oder wählen Sie eine Datei …"

#: ui/RunScriptEdit.qml:51
#, kde-format
msgid "Select executable file…"
msgstr "Ausführbare Datei auswählen …"

#: ui/RunScriptEdit.qml:66
#, kde-format
msgid "Select executable file"
msgstr "Ausführbare Datei auswählen"

#: ui/TimeDurationComboBox.qml:22
#, kde-format
msgctxt "@title:window"
msgid "Custom Duration"
msgstr ""

#~ msgctxt ""
#~ "List of recognized strings for 'minutes' in a time delay expression such "
#~ "as 'after 10 min'"
#~ msgid "m|min|mins|minutes"
#~ msgstr "m|Min|Minuten"

#~ msgctxt ""
#~ "List of recognized strings for 'seconds' in a time delay expression such "
#~ "as 'after 10 sec'"
#~ msgid "s|sec|secs|seconds"
#~ msgstr "s|Sek|Sekunden"

#~ msgctxt ""
#~ "Validator/extractor regular expression for a time delay number and unit, "
#~ "from e.g. 'after 10 min'. Uses recognized strings for minutes and seconds "
#~ "as %1 and %2."
#~ msgid "[^\\d]*(\\d+)\\s*(%1|%2)\\s*"
#~ msgstr "[^\\d]*(\\d+)\\s*(%1|%2)\\s*"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Thomas Reitelbach, Panagiotis Papadopoulos"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "tr@erdfunkstelle.de, pano_90@gmx.net"

#~ msgid ""
#~ "The KDE Power Management System will now generate a set of defaults based "
#~ "on your computer's capabilities. This will also erase all existing "
#~ "modifications you made. Are you sure you want to continue?"
#~ msgstr ""
#~ "Das KDE-Energieverwaltungssystem wird nun einen Satz Voreinstellungen "
#~ "erstellen, basierend auf den Fähigkeiten Ihres Rechners. Daber werden "
#~ "alle bisher getätigten Änderungen zurückgesetzt. Sind Sie sicher, dass "
#~ "Sie fortfahren möchten?"

#~ msgid "Restore Default Profiles"
#~ msgstr "Standardprofile wiederherstellen"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "Der Energieverwaltungsdienst scheint nicht zu laufen.\n"
#~ "Das können Sie ändern, indem Sie ihn starten oder unter \"Starten und "
#~ "Beenden\" einrichten."

#~ msgid "Power Profiles Configuration"
#~ msgstr "Einrichtung der Energieprofile"

#~ msgid "A profile configurator for KDE Power Management System"
#~ msgstr "Ein Modul zur Einrichtung der Energieverwaltung von KDE"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "©, 2010 Dario Freddi"

#~ msgid ""
#~ "From this module, you can manage KDE Power Management System's power "
#~ "profiles, by tweaking existing ones or creating new ones."
#~ msgstr ""
#~ "Mit diesem Modul können die Energieprofile des Energieverwaltungssystems "
#~ "von KDE durch Anpassen vorhandener oder Erstellen neuer Profile verwaltet "
#~ "werden."

#~ msgid "Dario Freddi"
#~ msgstr "Dario Freddi"

#~ msgid "Maintainer"
#~ msgstr "Betreuer"

#~ msgid "Please enter a name for the new profile:"
#~ msgstr "Bitte geben Sie einen Namen für das neue Profil ein:"

#~ msgid "The name for the new profile"
#~ msgstr "Der Name für das neue Profil"

#~ msgid "Enter here the name for the profile you are creating"
#~ msgstr "Geben Sie den Namen für das Profil ein, das Sie gerade erstellen."

#~ msgid "Please enter a name for this profile:"
#~ msgstr "Bitte geben Sie einen Namen für dieses Profil ein."

#~ msgid "Export Power Management Profiles"
#~ msgstr "Energieverwaltungsprofile exportieren"

#~ msgid ""
#~ "The current profile has not been saved.\n"
#~ "Do you want to save it?"
#~ msgstr ""
#~ "Das aktuelle Profil ist noch nicht gespeichert worden.\n"
#~ "Möchten Sie es nun speichern?"

#~ msgid "Save Profile"
#~ msgstr "Profil speichern"

#~ msgid "New Profile"
#~ msgstr "Neues Profil"

#~ msgid "Delete Profile"
#~ msgstr "Profil löschen"

#~ msgid "Import Profiles"
#~ msgstr "Profile importieren"

#~ msgid "Export Profiles"
#~ msgstr "Profile exportieren"

#~ msgid "Edit Profile"
#~ msgstr "Profil bearbeiten"
