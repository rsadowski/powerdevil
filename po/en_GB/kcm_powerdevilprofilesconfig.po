# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Andrew Coles <andrew_coles@yahoo.co.uk>, 2010.
# SPDX-FileCopyrightText: 2014, 2020, 2024 Steve Allewell <steve.allewell@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-18 00:41+0000\n"
"PO-Revision-Date: 2024-05-24 17:29+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.02.2\n"

#: PowerKCM.cpp:455
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "The Power Management Service appears not to be running."

#: ui/DurationPromptDialog.qml:130
#, kde-format
msgctxt "The unit of the time input field"
msgid "millisecond"
msgid_plural "milliseconds"
msgstr[0] "millisecond"
msgstr[1] "milliseconds"

#: ui/DurationPromptDialog.qml:132
#, kde-format
msgctxt "The unit of the time input field"
msgid "second"
msgid_plural "seconds"
msgstr[0] "second"
msgstr[1] "seconds"

#: ui/DurationPromptDialog.qml:134
#, kde-format
msgctxt "The unit of the time input field"
msgid "minute"
msgid_plural "minutes"
msgstr[0] "minute"
msgstr[1] "minutes"

#: ui/DurationPromptDialog.qml:136
#, kde-format
msgctxt "The unit of the time input field"
msgid "hour"
msgid_plural "hours"
msgstr[0] "hour"
msgstr[1] "hours"

#: ui/DurationPromptDialog.qml:138
#, kde-format
msgctxt "The unit of the time input field"
msgid "day"
msgid_plural "days"
msgstr[0] "day"
msgstr[1] "days"

#: ui/DurationPromptDialog.qml:140
#, kde-format
msgctxt "The unit of the time input field"
msgid "week"
msgid_plural "weeks"
msgstr[0] "week"
msgstr[1] "weeks"

#: ui/DurationPromptDialog.qml:142
#, kde-format
msgctxt "The unit of the time input field"
msgid "month"
msgid_plural "months"
msgstr[0] "month"
msgstr[1] "months"

#: ui/DurationPromptDialog.qml:144
#, kde-format
msgctxt "The unit of the time input field"
msgid "year"
msgid_plural "years"
msgstr[0] "year"
msgstr[1] "years"

#: ui/DurationPromptDialog.qml:171
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "milliseconds"
msgstr "milliseconds"

#: ui/DurationPromptDialog.qml:173
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "seconds"
msgstr "seconds"

#: ui/DurationPromptDialog.qml:175
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "minutes"
msgstr "minutes"

#: ui/DurationPromptDialog.qml:177
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "hours"
msgstr "hours"

#: ui/DurationPromptDialog.qml:179
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "days"
msgstr "days"

#: ui/DurationPromptDialog.qml:181
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "weeks"
msgstr "weeks"

#: ui/DurationPromptDialog.qml:183
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "months"
msgstr "months"

#: ui/DurationPromptDialog.qml:185
#, kde-format
msgctxt "@text:radiobutton Unit of the time input field"
msgid "years"
msgstr "years"

#: ui/GlobalConfig.qml:16
#, kde-format
msgctxt "@title"
msgid "Advanced Power Settings"
msgstr "Advanced Power Settings"

#: ui/GlobalConfig.qml:21
#, kde-format
msgctxt ""
"Percentage value example, used for formatting battery levels in the power "
"management settings"
msgid "10%"
msgstr "10%"

#: ui/GlobalConfig.qml:57
#, kde-format
msgctxt "@title:group"
msgid "Battery Levels"
msgstr "Battery Levels"

#: ui/GlobalConfig.qml:65
#, kde-format
msgctxt ""
"@label:spinbox Low battery level percentage for the main power supply battery"
msgid "&Low level:"
msgstr "&Low level:"

#: ui/GlobalConfig.qml:73
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Low battery level"
msgstr "Low battery level"

#: ui/GlobalConfig.qml:98
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The battery charge will be considered low when it drops to this level. "
"Settings for low battery will be used instead of regular battery settings."
msgstr ""
"The battery charge will be considered low when it drops to this level. "
"Settings for low battery will be used instead of regular battery settings."

#: ui/GlobalConfig.qml:105
#, kde-format
msgctxt ""
"@label:spinbox Critical battery level percentage for the main power supply "
"battery"
msgid "Cr&itical level:"
msgstr "Cr&itical level:"

#: ui/GlobalConfig.qml:113
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Critical battery level"
msgstr "Critical battery level"

#: ui/GlobalConfig.qml:138
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The battery charge will be considered critical when it drops to this level. "
"After a brief warning, the system will automatically suspend or shut down, "
"according to the configured critical battery level action."
msgstr ""
"The battery charge will be considered critical when it drops to this level. "
"After a brief warning, the system will automatically suspend or shut down, "
"according to the configured critical battery level action."

#: ui/GlobalConfig.qml:147
#, kde-format
msgctxt "@info:status"
msgid ""
"The action you had previously configured for reaching the critical battery "
"level is now unsupported on your system. Please select a different one."
msgstr ""
"The action you had previously configured for reaching the critical battery "
"level is now unsupported on your system. Please select a different one."

#: ui/GlobalConfig.qml:154
#, kde-format
msgctxt ""
"@label:combobox Power action such as sleep/hibernate that will be executed "
"when the critical battery level is reached"
msgid "A&t critical level:"
msgstr "A&t critical level:"

#: ui/GlobalConfig.qml:156
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action performed at critical battery level"
msgstr "Action performed at critical battery level"

#: ui/GlobalConfig.qml:181
#, kde-format
msgctxt "@label:spinbox Low battery level percentage for peripheral devices"
msgid "Low level for peripheral d&evices:"
msgstr "Low level for peripheral d&evices:"

#: ui/GlobalConfig.qml:189
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Low battery level for peripheral devices"
msgstr "Low battery level for peripheral devices"

#: ui/GlobalConfig.qml:214
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"The battery charge for peripheral devices will be considered low when it "
"reaches this level."
msgstr ""
"The battery charge for peripheral devices will be considered low when it "
"reaches this level."

#: ui/GlobalConfig.qml:222
#, kde-format
msgctxt "@title:group"
msgid "Charge Limit"
msgstr "Charge Limit"

#: ui/GlobalConfig.qml:233
#, kde-format
msgctxt "@label:checkbox"
msgid "&Battery protection:"
msgstr "&Battery protection:"

#: ui/GlobalConfig.qml:234
#, kde-format
msgctxt "@text:checkbox"
msgid "Limit the maximum battery charge"
msgstr "Limit the maximum battery charge"

#: ui/GlobalConfig.qml:242
#, kde-format
msgctxt ""
"@label:spinbox Battery will stop charging when this charge percentage is "
"reached"
msgid "&Stop charging at:"
msgstr "&Stop charging at:"

#: ui/GlobalConfig.qml:268
#, kde-format
msgctxt ""
"@label:spinbox Battery will start charging again when this charge percentage "
"is reached, after having hit the stop-charging threshold earlier"
msgid "Start charging once &below:"
msgstr "Start charging once &below:"

#: ui/GlobalConfig.qml:325
#, kde-format
msgctxt "@info:status"
msgid ""
"You might have to disconnect and re-connect the power source to start "
"charging the battery again."
msgstr ""
"You might have to disconnect and re-connect the power source to start "
"charging the battery again."

#: ui/GlobalConfig.qml:336
#, kde-format
msgctxt "@info"
msgid ""
"Regularly charging the battery close to 100%, or fully discharging it, may "
"accelerate deterioration of battery health. By limiting the maximum battery "
"charge, you can help extend the battery lifespan."
msgstr ""
"Regularly charging the battery close to 100%, or fully discharging it, may "
"accelerate deterioration of battery health. By limiting the maximum battery "
"charge, you can help extend the battery lifespan."

#: ui/GlobalConfig.qml:344
#, kde-format
msgctxt ""
"@title:group Miscellaneous power management settings that didn't fit "
"elsewhere"
msgid "Other Settings"
msgstr "Other Settings"

#: ui/GlobalConfig.qml:352
#, kde-format
msgctxt "@label:checkbox"
msgid "&Media playback:"
msgstr "&Media playback:"

#: ui/GlobalConfig.qml:353
#, kde-format
msgctxt "@text:checkbox"
msgid "Pause media players when suspending"
msgstr "Pause media players when suspending"

#: ui/GlobalConfig.qml:366
#, kde-format
msgctxt "@label:button"
msgid "Related pages:"
msgstr "Related pages:"

#: ui/GlobalConfig.qml:381
#, kde-format
msgctxt "@text:button Name of KCM, plus Power Management notification category"
msgid "Notifications: Power Management"
msgstr "Notifications: Power Management"

#: ui/GlobalConfig.qml:383
#, kde-format
msgid "Open \"Notifications\" settings page, \"Power Management\" section"
msgstr "Open \"Notifications\" settings page, \"Power Management\" section"

#: ui/GlobalConfig.qml:390
#, kde-format
msgctxt "@text:button Name of KCM"
msgid "Desktop Session"
msgstr "Desktop Session"

#: ui/GlobalConfig.qml:391
#, kde-format
msgid "Open \"Desktop Session\" settings page"
msgstr "Open \"Desktop Session\" settings page"

#: ui/GlobalConfig.qml:398
#, kde-format
msgctxt "@text:button Name of KCM"
msgid "Activities"
msgstr "Activities"

#: ui/GlobalConfig.qml:399
#, kde-format
msgid "Open \"Activities\" settings page"
msgstr "Open \"Activities\" settings page"

#: ui/main.qml:19
#, kde-format
msgctxt "@action:button"
msgid "Advanced Power &Settings…"
msgstr "Advanced Power &Settings…"

#: ui/main.qml:43
#, kde-format
msgctxt "@text:placeholdermessage"
msgid "Power Management settings could not be loaded"
msgstr "Power Management settings could not be loaded"

#: ui/main.qml:65
#, kde-format
msgid "On AC Power"
msgstr "On AC Power"

#: ui/main.qml:66
#, kde-format
msgid "On Battery"
msgstr "On Battery"

#: ui/main.qml:67
#, kde-format
msgid "On Low Battery"
msgstr "On Low Battery"

#: ui/ProfileConfig.qml:24
#, kde-format
msgctxt ""
"Percentage value example, used for formatting brightness levels in the power "
"management settings"
msgid "10%"
msgstr "10%"

#: ui/ProfileConfig.qml:33
#, kde-format
msgctxt "@title:group"
msgid "Suspend Session"
msgstr "Suspend Session"

#: ui/ProfileConfig.qml:48
#, kde-format
msgctxt "@info:status"
msgid ""
"The action you had previously configured for after a period of inactivity is "
"now unsupported on your system. Please select a different one."
msgstr ""
"The action you had previously configured for after a period of inactivity is "
"now unsupported on your system. Please select a different one."

#: ui/ProfileConfig.qml:55
#, kde-format
msgctxt ""
"@label:combobox Suspend action such as sleep/hibernate to perform when the "
"system is idle"
msgid "A&fter a period of inactivity:"
msgstr "A&fter a period of inactivity:"

#: ui/ProfileConfig.qml:63
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action to perform when the system is idle"
msgstr "Action to perform when the system is idle"

#: ui/ProfileConfig.qml:95
#, kde-format
msgctxt "@option:combobox"
msgid "after %1 second"
msgid_plural "after %1 seconds"
msgstr[0] "after %1 second"
msgstr[1] "after %1 seconds"

#: ui/ProfileConfig.qml:97
#, kde-format
msgctxt "@option:combobox"
msgid "after %1 minute"
msgid_plural "after %1 minutes"
msgstr[0] "after %1 minute"
msgstr[1] "after %1 minutes"

#: ui/ProfileConfig.qml:110 ui/ProfileConfig.qml:383 ui/ProfileConfig.qml:526
#: ui/ProfileConfig.qml:862
#, kde-format
msgctxt ""
"@option:combobox Choose a custom value outside the list of preset values"
msgid "Custom…"
msgstr "Custom…"

#: ui/ProfileConfig.qml:148
#, kde-format
msgctxt "@info:status"
msgid ""
"The action you had previously configured for when the power button is "
"pressed is now unsupported on your system. Please select a different one."
msgstr ""
"The action you had previously configured for when the power button is "
"pressed is now unsupported on your system. Please select a different one."

#: ui/ProfileConfig.qml:155
#, kde-format
msgctxt ""
"@label:combobox Suspend action such as sleep/hibernate to perform when the "
"power button is pressed"
msgid "When &power button pressed:"
msgstr "When &power button pressed:"

#: ui/ProfileConfig.qml:157
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action to perform when the power button is pressed"
msgstr "Action to perform when the power button is pressed"

#: ui/ProfileConfig.qml:187
#, kde-format
msgctxt "@info:status"
msgid ""
"The action you had previously configured for when the lid is closed is now "
"unsupported on your system. Please select a different one."
msgstr ""
"The action you had previously configured for when the lid is closed is now "
"unsupported on your system. Please select a different one."

#: ui/ProfileConfig.qml:194
#, kde-format
msgctxt ""
"@label:combobox Suspend action such as sleep/hibernate to perform when the "
"power button is pressed"
msgid "When laptop &lid closed:"
msgstr "When laptop &lid closed:"

#: ui/ProfileConfig.qml:196
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "Action to perform when the laptop lid is closed"
msgstr "Action to perform when the laptop lid is closed"

#: ui/ProfileConfig.qml:225
#, kde-format
msgctxt ""
"@text:checkbox Trigger laptop lid action even when an external monitor is "
"connected"
msgid "Even when an external monitor is connected"
msgstr "Even when an external monitor is connected"

#: ui/ProfileConfig.qml:227
#, kde-format
msgid "Perform laptop lid action even when an external monitor is connected"
msgstr "Perform laptop lid action even when an external monitor is connected"

#: ui/ProfileConfig.qml:245
#, kde-format
msgctxt "@info:status"
msgid ""
"The sleep mode you had previously configured is now unsupported on your "
"system. Please select a different one."
msgstr ""
"The sleep mode you had previously configured is now unsupported on your "
"system. Please select a different one."

#: ui/ProfileConfig.qml:252
#, kde-format
msgctxt ""
"@label:combobox Sleep mode selection - suspend to memory, disk or both"
msgid "When sleeping, enter:"
msgstr "When sleeping, enter:"

#: ui/ProfileConfig.qml:254
#, kde-format
msgctxt "@accessible:name:combobox"
msgid "When sleeping, enter this power-save mode"
msgstr "When sleeping, enter this power-save mode"

#: ui/ProfileConfig.qml:303
#, kde-format
msgctxt "@title:group"
msgid "Display and Brightness"
msgstr "Display and Brightness"

#: ui/ProfileConfig.qml:313
#, kde-format
msgctxt "@label:slider Brightness level"
msgid "Change scr&een brightness:"
msgstr "Change scr&een brightness:"

#: ui/ProfileConfig.qml:359
#, kde-format
msgctxt "@label:combobox Dim screen after X minutes"
msgid "Di&m automatically:"
msgstr "Di&m automatically:"

#: ui/ProfileConfig.qml:362
#, kde-format
msgctxt "@label:spinbox Dim screen after X minutes"
msgid "Di&m automatically after:"
msgstr "Di&m automatically after:"

#: ui/ProfileConfig.qml:367
#, kde-format
msgctxt "@option:combobox"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 second"
msgstr[1] "%1 seconds"

#: ui/ProfileConfig.qml:369
#, kde-format
msgctxt "@option:combobox"
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minute"
msgstr[1] "%1 minutes"

#: ui/ProfileConfig.qml:376
#, kde-format
msgctxt "@option:combobox Dim screen automatically"
msgid "Never"
msgstr "Never"

#: ui/ProfileConfig.qml:428
#, kde-format
msgctxt "@info:status"
msgid ""
"The screen will not be dimmed because it is configured to turn off sooner."
msgstr ""
"The screen will not be dimmed because it is configured to turn off sooner."

#: ui/ProfileConfig.qml:433
#, kde-format
msgctxt "@label:combobox After X minutes"
msgid "&Turn off screen:"
msgstr "&Turn off screen:"

#: ui/ProfileConfig.qml:446
#, kde-format
msgctxt "@label:spinbox After X minutes"
msgid "Turn off screen after:"
msgstr "Turn off screen after:"

#: ui/ProfileConfig.qml:450
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 second"
msgstr[1] "%1 seconds"

#: ui/ProfileConfig.qml:452
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minute"
msgstr[1] "%1 minutes"

#: ui/ProfileConfig.qml:459
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "Never"
msgstr "Never"

#: ui/ProfileConfig.qml:466
#, kde-format
msgctxt ""
"@option:combobox Choose a custom value outside the list of preset values "
"(caution: watch for string length)"
msgid "Custom…"
msgstr "Custom…"

#: ui/ProfileConfig.qml:506
#, kde-format
msgctxt "@label:spinbox After X minutes"
msgid "When locked, turn off screen after:"
msgstr "When locked, turn off screen after:"

#: ui/ProfileConfig.qml:510
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "When locked: %1 second"
msgid_plural "When locked: %1 seconds"
msgstr[0] "When locked: %1 second"
msgstr[1] "When locked: %1 seconds"

#: ui/ProfileConfig.qml:512
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "When locked: %1 minute"
msgid_plural "When locked: %1 minutes"
msgstr[0] "When locked: %1 minute"
msgstr[1] "When locked: %1 minutes"

#: ui/ProfileConfig.qml:519
#, kde-format
msgctxt ""
"@option:combobox Turn off screen after X minutes regardless of lock screen "
"(caution: watch for string length)"
msgid "When locked and unlocked"
msgstr "When locked and unlocked"

#: ui/ProfileConfig.qml:521
#, kde-format
msgctxt "@option:combobox Turn off screen (caution: watch for string length)"
msgid "When locked: Immediately"
msgstr "When locked: Immediately"

#: ui/ProfileConfig.qml:560
#, kde-format
msgctxt "@label:slider Brightness level"
msgid "Change key&board brightness:"
msgstr "Change key&board brightness:"

#: ui/ProfileConfig.qml:607
#, kde-format
msgctxt "@title:group"
msgid "Other Settings"
msgstr "Other Settings"

#: ui/ProfileConfig.qml:615
#, kde-format
msgctxt ""
"@label:combobox Power Save, Balanced or Performance profile - same options "
"as in the Battery applet"
msgid "Switch to po&wer profile:"
msgstr "Switch to po&wer profile:"

#: ui/ProfileConfig.qml:619
#, kde-format
msgctxt ""
"@accessible:name:combobox Power Save, Balanced or Performance profile - same "
"options as in the Battery applet"
msgid "Switch to power profile"
msgstr "Switch to power profile"

#: ui/ProfileConfig.qml:646
#, kde-format
msgctxt "@label:button"
msgid "Run custom script:"
msgstr "Run custom script:"

#: ui/ProfileConfig.qml:656
#, kde-format
msgctxt ""
"@text:button Determine what will trigger a script command to run in this "
"power state"
msgid "Choose run conditions…"
msgstr "Choose run conditions…"

#: ui/ProfileConfig.qml:658
#, kde-format
msgctxt "@accessible:name:button"
msgid "Choose run conditions for script command"
msgstr "Choose run conditions for script command"

#: ui/ProfileConfig.qml:677
#, kde-format
msgctxt ""
"@text:action:menu Script command to execute for power state (On AC Power, On "
"Battery, ...)"
msgid "When entering \"%1\" state"
msgstr "When entering \"%1\" state"

#: ui/ProfileConfig.qml:695
#, kde-format
msgctxt ""
"@text:action:menu Script command to execute for power state (On AC Power, On "
"Battery, ...)"
msgid "When exiting \"%1\" state"
msgstr "When exiting \"%1\" state"

#: ui/ProfileConfig.qml:713
#, kde-format
msgctxt "@text:action:menu Script command to execute"
msgid "After a period of inactivity"
msgstr "After a period of inactivity"

#: ui/ProfileConfig.qml:736
#, kde-format
msgctxt ""
"@label:textfield Script command to execute for power state (On AC Power, On "
"Battery, ...)"
msgid "When e&ntering \"%1\" state:"
msgstr "When e&ntering \"%1\" state:"

#: ui/ProfileConfig.qml:740
#, kde-format
msgctxt "@label:textfield for power state (On AC Power, On Battery, ...)"
msgid "Script command when entering \"%1\" state"
msgstr "Script command when entering \"%1\" state"

#: ui/ProfileConfig.qml:771
#, kde-format
msgctxt ""
"@label:textfield Script command to execute for power state (On AC Power, On "
"Battery, ...)"
msgid "When e&xiting \"%1\" state:"
msgstr "When e&xiting \"%1\" state:"

#: ui/ProfileConfig.qml:775
#, kde-format
msgctxt "@label:textfield for power state (On AC Power, On Battery, ...)"
msgid "Script command when exiting \"%1\" state"
msgstr "Script command when exiting \"%1\" state"

#: ui/ProfileConfig.qml:806
#, kde-format
msgctxt "@label:textfield Script command to execute"
msgid "After a period of inacti&vity:"
msgstr "After a period of inacti&vity:"

#: ui/ProfileConfig.qml:808
#, kde-format
msgctxt "@@accessible:name:textfield"
msgid "Script command after a period of inactivity"
msgstr "Script command after a period of inactivity"

#: ui/ProfileConfig.qml:837
#, kde-format
msgctxt "@accessible:name:spinbox"
msgid "Period of inactivity until the script command executes"
msgstr "Period of inactivity until the script command executes"

#: ui/ProfileConfig.qml:842
#, kde-format
msgctxt "@label:spinbox After X minutes"
msgid "Execute script after:"
msgstr "Execute script after:"

#: ui/ProfileConfig.qml:847
#, kde-format
msgctxt "@option:combobox"
msgid "after %1 seconds"
msgid_plural "after %1 seconds"
msgstr[0] "after %1 seconds"
msgstr[1] "after %1 seconds"

#: ui/ProfileConfig.qml:849
#, kde-format
msgctxt "@option:combobox"
msgid "after %1 minutes"
msgid_plural "after %1 minutes"
msgstr[0] "after %1 minutes"
msgstr[1] "after %1 minutes"

#: ui/RunScriptEdit.qml:33
#, kde-format
msgid "Enter command or select file…"
msgstr "Enter command or select file…"

#: ui/RunScriptEdit.qml:51
#, kde-format
msgid "Select executable file…"
msgstr "Select executable file…"

#: ui/RunScriptEdit.qml:66
#, kde-format
msgid "Select executable file"
msgstr "Select executable file"

#: ui/TimeDurationComboBox.qml:22
#, kde-format
msgctxt "@title:window"
msgid "Custom Duration"
msgstr "Custom Duration"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Andrew Coles, Steve Allewell"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "andrew_coles@yahoo.co.uk, steve.allewell@gmail.com"

#~ msgid ""
#~ "The KDE Power Management System will now generate a set of defaults based "
#~ "on your computer's capabilities. This will also erase all existing "
#~ "modifications you made. Are you sure you want to continue?"
#~ msgstr ""
#~ "The KDE Power Management System will now generate a set of defaults based "
#~ "on your computer's capabilities. This will also erase all existing "
#~ "modifications you made. Are you sure you want to continue?"

#~ msgid "Restore Default Profiles"
#~ msgstr "Restore Default Profiles"

#~ msgid ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""
#~ msgstr ""
#~ "The Power Management Service appears not to be running.\n"
#~ "This can be solved by starting or scheduling it inside \"Startup and "
#~ "Shutdown\""

#~ msgid "Power Profiles Configuration"
#~ msgstr "Power Profiles Configuration"

#~ msgid "A profile configurator for KDE Power Management System"
#~ msgstr "A profile configurator for KDE Power Management System"

#~ msgid "(c), 2010 Dario Freddi"
#~ msgstr "(c), 2010 Dario Freddi"

#~ msgid ""
#~ "From this module, you can manage KDE Power Management System's power "
#~ "profiles, by tweaking existing ones or creating new ones."
#~ msgstr ""
#~ "From this module, you can manage KDE Power Management System's power "
#~ "profiles, by tweaking existing ones or creating new ones."

#~ msgid "Dario Freddi"
#~ msgstr "Dario Freddi"

#~ msgid "Maintainer"
#~ msgstr "Maintainer"

#~ msgid "Please enter a name for the new profile:"
#~ msgstr "Please enter a name for the new profile:"

#~ msgid "The name for the new profile"
#~ msgstr "The name for the new profile"

#~ msgid "Enter here the name for the profile you are creating"
#~ msgstr "Enter here the name for the profile you are creating"

#~ msgid "Please enter a name for this profile:"
#~ msgstr "Please enter a name for this profile:"

#~ msgid "Export Power Management Profiles"
#~ msgstr "Export Power Management Profiles"

#~ msgid ""
#~ "The current profile has not been saved.\n"
#~ "Do you want to save it?"
#~ msgstr ""
#~ "The current profile has not been saved.\n"
#~ "Do you want to save it?"

#~ msgid "Save Profile"
#~ msgstr "Save Profile"

#~ msgid "New Profile"
#~ msgstr "New Profile"

#~ msgid "Delete Profile"
#~ msgstr "Delete Profile"

#~ msgid "Import Profiles"
#~ msgstr "Import Profiles"

#~ msgid "Export Profiles"
#~ msgstr "Export Profiles"

#~ msgid "Edit Profile"
#~ msgstr "Edit Profile"
